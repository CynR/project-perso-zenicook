package com.cyn.zenicook.services;

import com.cyn.zenicook.controllers.representation.ingredient.NewIngredient;
import com.cyn.zenicook.controllers.representation.recipe.NewRecipe;
import com.cyn.zenicook.domain.Ingredient;
import com.cyn.zenicook.domain.Recipe;
import com.cyn.zenicook.repositories.RecipeRepository;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Component
public class RecipeService {
    private RecipeRepository recipeRepository;
    private IdGenerator idGenerator;
    private IngredientService ingredientService;
    private static final Logger log = LoggerFactory.getLogger(RecipeService.class);

    public RecipeService(RecipeRepository recipeRepository, IdGenerator idGenerator, IngredientService ingredientService) {
        this.recipeRepository = recipeRepository;
        this.idGenerator = idGenerator;
        this.ingredientService = ingredientService;
    }

    public Optional<Recipe> getById(String id) {
        return this.recipeRepository.findById(id);
    }

    public List<Recipe> getAllRecipes() {
        return (List<Recipe>) this.recipeRepository.findAll();
    }

    public Optional<Recipe> createRecipe (NewRecipe body) {
        if (body.getName() != null  || body.getIngredientList() != null) {
            List<NewIngredient> listNewIngredients = body.getIngredientList();
            List<Ingredient> listToImplement = new ArrayList<>();
            for (NewIngredient ing : listNewIngredients) {
                listToImplement.add(new Ingredient(this.idGenerator.generateNewId(), ing.getNameIngredient()));
            }
            Recipe recipe = new Recipe(
                    this.idGenerator.generateNewId(),
                    body.getName(),
                    listToImplement,
                    body.getCooking_time(),
                    body.getPreparation_time(),
                    body.getDifficulty(),
                    body.getServings(),
                    body.getPicture_url()
            );
            this.recipeRepository.save(recipe);
            return Optional.of(recipe);
        }
        throw new IllegalArgumentException("Unsuccessful creation. Please insert valid and complete information");
    }

    public Optional<Recipe> deleteRecipe(String id) {
        Optional<Recipe> recipe = this.recipeRepository.findById(id);
        if (recipe.isPresent()) {
            this.recipeRepository.deleteById(id);
            return recipe;
        }
        throw new IllegalArgumentException("Invalid process, the Id provided does not correspond to any existent recipe");
    }
    public Optional<Recipe> modifyRecipe (String id, NewRecipe body) {
        Optional<Recipe> recipeToModify = this.recipeRepository.findById(id); //get recipe from repo
        List<NewIngredient> listNewIngredients = body.getIngredientList();
        List<Ingredient> listToImplement = new ArrayList<>();
        for (NewIngredient ing : listNewIngredients) {
            listToImplement.add(new Ingredient(this.idGenerator.generateNewId(), ing.getNameIngredient()));
        }
        if (recipeToModify.isPresent()) { //modify values in case they are inserted and not nul
            if (!body.getName().equals("") && body.getName() != null) {
                recipeToModify.get().setName(body.getName());
            } else {
                log.info("Recipe's name remains unchanged");
            }
            if (body.getIngredientList() != null) {
                recipeToModify.get().setListOfIngredients(listToImplement);
            } else {
                log.info("Recipe's content remains unchanged");
            }
            this.recipeRepository.save(recipeToModify.get());
        } else {
            throw new IllegalArgumentException("Invalid Id. Recipe remains unchanged");
        }
        return recipeToModify;
    }
    //end of class Recipe Service
}
