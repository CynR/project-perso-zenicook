package com.cyn.zenicook.services;

import com.cyn.zenicook.controllers.representation.ingredient.NewIngredient;
import com.cyn.zenicook.domain.Ingredient;
import com.cyn.zenicook.repositories.IngredientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class IngredientService {
    private IngredientRepository ingredientRepository;
    private IdGenerator idGenerator;
    private static final Logger log = LoggerFactory.getLogger(IngredientService.class);

    public IngredientService(IngredientRepository ingredientRepository, IdGenerator idGenerator) {
        this.ingredientRepository = ingredientRepository;
        this.idGenerator = idGenerator;
    }

    public Optional<Ingredient> getById(String id) {
        return this.ingredientRepository.findById(id);
    }

    public List<Ingredient> getAllIngredients() {
        return (List<Ingredient>) this.ingredientRepository.findAll();
    }

    public Optional<Ingredient> createIngredient(NewIngredient body) {
        if (!body.getNameIngredient().equals("")) {
            Ingredient ingredient = new Ingredient(
                    this.idGenerator.generateNewId(),
                    body.getNameIngredient()
            );
            this.ingredientRepository.save(ingredient);
            return Optional.of(ingredient);
        }
        throw new IllegalArgumentException("Unsuccessful creation. Please insert valid and complete information");
    }

    public Optional<Ingredient> deleteIngredient(String id) {
        Optional<Ingredient> ingredient = this.ingredientRepository.findById(id);
        if (ingredient.isPresent()) {
            this.ingredientRepository.deleteById(id);
            return ingredient;
        }
        throw new IllegalArgumentException("Invalid process, the Id provided does not correspond to any existent ingredient");
    }

    public Optional<Ingredient> modigyIngredient(String id, NewIngredient body) {
        Optional<Ingredient> ingredientToModify = this.ingredientRepository.findById(id);
        if (ingredientToModify.isPresent()) {
            if (!body.getNameIngredient().equals("") && body.getNameIngredient() != null) {
                ingredientToModify.get().setIngredient_name(body.getNameIngredient());
            }
            else{
                log.info("Ingredient's name remains unchanged");
            }
            this.ingredientRepository.save(ingredientToModify.get());
        } else {
            throw new IllegalArgumentException("Invalid Id. Ingredient remains unchanged");
        }
        return ingredientToModify;
    }
    // end of class Ingredient
}
