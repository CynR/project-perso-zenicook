package com.cyn.zenicook.domain;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table (name = "ingredients")
public class Ingredient {
    @Id
    private String id;

    private String ingredient_name;

    @ManyToMany(mappedBy="ingredient_name")
    private List<Recipe> listRecipes;

    public Ingredient(String id, String ingredient_name) {
        this.id = id;
        this.ingredient_name = ingredient_name;
    }

    public Ingredient() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIngredient_name() {
        return ingredient_name;
    }

    public void setIngredient_name(String ingredient_name) {
        this.ingredient_name = ingredient_name;
    }

    @Override
    public String toString() {
        return String.format(
                "Recipe[id=%d, name='%s', ingredient_name='%s]", id, ingredient_name);
    }

        @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ingredient)) return false;
        Ingredient that = (Ingredient) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getIngredient_name(), that.getIngredient_name());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getIngredient_name());
    }
}


