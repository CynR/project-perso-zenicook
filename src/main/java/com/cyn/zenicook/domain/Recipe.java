package com.cyn.zenicook.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "recipes")
public class Recipe implements Serializable {
    @Id
    private String id;
    private String name;
    @ManyToMany

    private List<Ingredient> ingredient_name;
    private int cooking_time;
    private int preparation_time;
    private int difficulty;
    private int servings;
    private String picture_url;


    protected Recipe() {}

    public Recipe(String id, String name, List<Ingredient> ingredient_name, int cooking_time,
                  int preparation_time, int difficulty, int servings, String picture_url) {
        this.id = id;
        this.name = name;
        this.ingredient_name = ingredient_name;
        this.cooking_time = cooking_time;
        this.preparation_time = preparation_time;
        this.difficulty = difficulty;
        this.servings = servings;
        this.picture_url = picture_url;
    }

    @Override
    public String toString() {
        return String.format(
                "Recipe[id=%d, name='%s', ingredient_name='%s', cooking_time='%s', preparation_time='%s'" +
                        "dificulty='%s', servings='%s', picture_url='%s']",
                id, name, ingredient_name, cooking_time, preparation_time, difficulty, servings, picture_url);
    }

    public int getCooking_time() {
        return cooking_time;
    }

    public void setCooking_time(int cooking_time) {
        this.cooking_time = cooking_time;
    }

    public int getPreparation_time() {
        return preparation_time;
    }

    public void setPreparation_time(int preparation_time) {
        this.preparation_time = preparation_time;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getServings() {
        return servings;
    }

    public void setServings(int servings) {
        this.servings = servings;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setListOfIngredients(List<Ingredient> ingredientList) {
        this.ingredient_name = ingredientList;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Ingredient> getListOfIngredients() {
        return ingredient_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Recipe)) return false;
        Recipe recipe = (Recipe) o;
        return getCooking_time() == recipe.getCooking_time() &&
                getPreparation_time() == recipe.getPreparation_time() &&
                getDifficulty() == recipe.getDifficulty() &&
                getServings() == recipe.getServings() &&
                Objects.equals(getId(), recipe.getId()) &&
                Objects.equals(getName(), recipe.getName()) &&
                Objects.equals(ingredient_name, recipe.ingredient_name) &&
                Objects.equals(getPicture_url(), recipe.getPicture_url());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), ingredient_name, getCooking_time(), getPreparation_time(), getDifficulty(), getServings(), getPicture_url());
    }
}
