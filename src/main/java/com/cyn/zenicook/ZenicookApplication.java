package com.cyn.zenicook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZenicookApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZenicookApplication.class, args);
	}

}
