package com.cyn.zenicook.controllers;

import com.cyn.zenicook.controllers.representation.recipe.DisplayableRecipeRepresentation;
import com.cyn.zenicook.controllers.representation.recipe.NewRecipe;
import com.cyn.zenicook.controllers.representation.recipe.RecipeRepresentationMapper;
import com.cyn.zenicook.domain.Recipe;
import com.cyn.zenicook.services.RecipeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.DataInput;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/recipes")
public class RecipeController {

    private RecipeService recipeService;
    private RecipeRepresentationMapper mapper;

    @Autowired
    public RecipeController(RecipeService recipeService, RecipeRepresentationMapper mapper) {
        this.recipeService = recipeService;
        this.mapper= mapper;

    }

    @GetMapping
    public List<DisplayableRecipeRepresentation> getAllRecipes(){
        return this.recipeService.getAllRecipes().stream()
                .map(this.mapper::mapToDislpayableRecipe)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<DisplayableRecipeRepresentation> getOneRecipe(@PathVariable("id") String id) {
        Optional<Recipe> recipe = this.recipeService.getById(id);
        return recipe
                .map(this.mapper::mapToDislpayableRecipe)
                .map(ResponseEntity :: ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<DisplayableRecipeRepresentation> deleteOneRecipe(@PathVariable("id") String id) {
        Optional<Recipe> recipe = this.recipeService.deleteRecipe(id);
        return recipe
                .map(this.mapper::mapToDislpayableRecipe)
                .map(ResponseEntity :: ok)
                .orElseGet(() -> ResponseEntity.noContent().build());
    }
    @PostMapping
    public ResponseEntity<DisplayableRecipeRepresentation> createOneRecipe(@RequestBody NewRecipe body)  {
        Optional<Recipe> recipe = this.recipeService.createRecipe(body);
        return recipe
                .map(this.mapper::mapToDislpayableRecipe)
                .map(ResponseEntity :: ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
    @PutMapping("/{id}")
    public ResponseEntity<DisplayableRecipeRepresentation> modifyRecipe(@PathVariable("id") String id,
                                                                         @RequestBody NewRecipe body) {
        Optional<Recipe> recipe = this.recipeService.modifyRecipe(id,body);
        return recipe
                .map(this.mapper::mapToDislpayableRecipe)
                .map(ResponseEntity :: ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }


}
