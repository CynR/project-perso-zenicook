package com.cyn.zenicook.controllers;

import com.cyn.zenicook.controllers.representation.ingredient.DisplayableIngredientRepresentation;
import com.cyn.zenicook.controllers.representation.ingredient.IngredientRepresentationMapper;
import com.cyn.zenicook.controllers.representation.ingredient.NewIngredient;
import com.cyn.zenicook.domain.Ingredient;
import com.cyn.zenicook.services.IngredientService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.swing.text.html.Option;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/ingredients")
public class IngredientController {

    private IngredientService ingredientService;
    private IngredientRepresentationMapper mapper;

    @Autowired
    public IngredientController(IngredientService ingredientService, IngredientRepresentationMapper mapper) {
        this.ingredientService = ingredientService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<DisplayableIngredientRepresentation> getAllIngredients() {
        return this.ingredientService.getAllIngredients().stream()
                .map(this.mapper::mapToDisplayableIngredient)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<DisplayableIngredientRepresentation> getOneIngredient(@PathVariable("id") String id) {
        Optional<Ingredient> ingredient = this.ingredientService.getById(id);
        return ingredient
                .map(this.mapper::mapToDisplayableIngredient)
                .map(ResponseEntity::ok)
                .orElseGet(()-> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<DisplayableIngredientRepresentation> createIngredient(@RequestBody NewIngredient body) {
        Optional<Ingredient> ingredient = this.ingredientService.createIngredient(body);
        return ingredient
                .map(this.mapper::mapToDisplayableIngredient)
                .map(ResponseEntity::ok)
                .orElseGet(()-> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<DisplayableIngredientRepresentation> deleteIngredient(@PathVariable("id") String id) {
        Optional<Ingredient> ingredient = this.ingredientService.deleteIngredient(id);
        return ingredient
                .map(this.mapper::mapToDisplayableIngredient)
                .map(ResponseEntity::ok)
                .orElseGet(()-> ResponseEntity.noContent().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<DisplayableIngredientRepresentation> modifyIngredient(@PathVariable("id") String id,
                                                                @RequestBody NewIngredient body) {
        Optional<Ingredient> ingredient = this.ingredientService.modigyIngredient(id, body);
        return ingredient
                .map(this.mapper::mapToDisplayableIngredient)
                .map(ResponseEntity::ok)
                .orElseGet(()-> ResponseEntity.noContent().build());
    }
}
