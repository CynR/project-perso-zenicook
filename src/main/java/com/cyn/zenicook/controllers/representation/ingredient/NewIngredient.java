package com.cyn.zenicook.controllers.representation.ingredient;

public class NewIngredient {
    private String nameIngredient;

    public String getNameIngredient() {
        return nameIngredient;
    }

    public void setNameIngredient(String nameIngredient) {
        this.nameIngredient = nameIngredient;
    }
}
