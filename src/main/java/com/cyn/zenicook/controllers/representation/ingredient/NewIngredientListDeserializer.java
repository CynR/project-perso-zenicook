package com.cyn.zenicook.controllers.representation.ingredient;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.List;

public class NewIngredientListDeserializer extends StdDeserializer<List<NewIngredient>> {

        public NewIngredientListDeserializer() {
            this(null);
        }

        public NewIngredientListDeserializer(Class<?> vc) {
            super(vc);
        }

        @Override
        public List<NewIngredient> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            return null;
        }
    }

