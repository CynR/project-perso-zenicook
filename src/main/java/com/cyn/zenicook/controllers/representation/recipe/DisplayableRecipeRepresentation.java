package com.cyn.zenicook.controllers.representation.recipe;

import com.cyn.zenicook.domain.Ingredient;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collection;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DisplayableRecipeRepresentation {
    private String id;
    private String name;
    private List<Ingredient> ingredientList;
    private int cooking_time;
    private int preparation_time;
    private int difficulty;
    private int servings;
    private String picture_url;

    public int getCooking_time() {
        return cooking_time;
    }

    public void setCooking_time(int cooking_time) {
        this.cooking_time = cooking_time;
    }

    public int getPreparation_time() {
        return preparation_time;
    }

    public void setPreparation_time(int preparation_time) {
        this.preparation_time = preparation_time;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getServings() {
        return servings;
    }

    public void setServings(int servings) {
        this.servings = servings;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ingredient> getDisplayableIngredientList() {
        return ingredientList;
    }

    public void getDisplayableIngredientList(List<Ingredient> ingredientList) {
        this.ingredientList = ingredientList;
    }
}
