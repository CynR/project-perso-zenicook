package com.cyn.zenicook.controllers.representation.ingredient;

import com.cyn.zenicook.domain.Ingredient;
import org.springframework.stereotype.Component;


@Component
public class IngredientRepresentationMapper {

    public DisplayableIngredientRepresentation mapToDisplayableIngredient(Ingredient ingredient) {
        DisplayableIngredientRepresentation result = new DisplayableIngredientRepresentation();
                result.setId(ingredient.getId());
                result.setNameIngredient(ingredient.getIngredient_name());
        return result;
    }

}
