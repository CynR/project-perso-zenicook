package com.cyn.zenicook.controllers.representation.recipe;

import com.cyn.zenicook.controllers.representation.ingredient.NewIngredient;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.List;

public class NewRecipeListDeserializer extends StdDeserializer<List<NewRecipe>> {

    public NewRecipeListDeserializer() {
        this(null);
    }

    public NewRecipeListDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public List<NewRecipe> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        return null;
    }
}
