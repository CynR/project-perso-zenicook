package com.cyn.zenicook.controllers.representation.recipe;

import com.cyn.zenicook.domain.Recipe;
import org.springframework.stereotype.Component;

@Component
public class RecipeRepresentationMapper {

    public DisplayableRecipeRepresentation mapToDislpayableRecipe(Recipe recipe) {
        DisplayableRecipeRepresentation result = new DisplayableRecipeRepresentation();
        result.setId(recipe.getId());
        result.setName(recipe.getName());
        //list ?
        result.getDisplayableIngredientList(recipe.getListOfIngredients());
        result.setCooking_time(recipe.getCooking_time());
        result.setPreparation_time(recipe.getPreparation_time());
        result.setDifficulty(recipe.getDifficulty());
        result.setServings(recipe.getServings());
        result.setPicture_url(recipe.getPicture_url());
        return result;
    }
}
