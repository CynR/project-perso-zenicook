package com.cyn.zenicook.controllers.representation.recipe;

import com.cyn.zenicook.controllers.representation.ingredient.IngredientRepresentationMapper;
import com.cyn.zenicook.controllers.representation.ingredient.NewIngredient;
import com.cyn.zenicook.controllers.representation.ingredient.NewIngredientListDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = false)
public class NewRecipe {

    private String name;
    // ATTENTION !!! List<NewIngredient>
    @JsonDeserialize(using = NewIngredientListDeserializer.class)
    @JsonIgnore
    private List<NewIngredient> ingredientList;
    private int cooking_time;
    private int preparation_time;
    private int difficulty;
    private int servings;
    private String picture_url;

    public int getCooking_time() {
        return cooking_time;
    }

    public void setCooking_time(int cooking_time) {
        this.cooking_time = cooking_time;
    }

    public int getPreparation_time() {
        return preparation_time;
    }

    public void setPreparation_time(int preparation_time) {
        this.preparation_time = preparation_time;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getServings() {
        return servings;
    }

    public void setServings(int servings) {
        this.servings = servings;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<NewIngredient> getIngredientList() {
        List<NewIngredient> ing = this.ingredientList;
        return ing;
    }

    public void setIngredientList(List<NewIngredient> newIngredientList) {
        this.ingredientList = ingredientList;
    }
}
