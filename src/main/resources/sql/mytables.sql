drop table if exists recipes;
create table recipes
(
id 			text primary key,
name 			text not null,
preparation_time int not null,
cooking_time	 int not null,
difficulty		 int not null,
servings		 int not null,
picture_url		text
);

drop table if exists ingredients;
create table ingredients
(
id text primary key,
ingredient_name text not null
);

drop table if exists measures;
create table measures
(
	id_ingredient	text references ingredients (id),
	id_recipe	text references recipes (id),
	unit		text not null,
	quantity	int not null,
	PRIMARY KEY (id_ingredient, id_recipe)
);





