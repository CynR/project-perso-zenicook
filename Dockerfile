FROM maven:3.6.3-jdk-13 as build
WORKDIR /src
COPY pom.xml .
RUN mvn dependency:go-offline
COPY . .
RUN mvn clean package -Dmaven.test.skip=true

FROM openjdk:13.0-slim
COPY --from=build /src/target/*.jar ./

EXPOSE 8080
CMD java -jar *.jar

